package nl.bioinf.jklimp.QuarantinePredictor;

import weka.classifiers.meta.CostSensitiveClassifier;
import weka.core.Instances;
import weka.core.SerializationHelper;

import java.io.File;

/**
 * Class which classifies the instances from ProcessCLI
 */


public class ClassifyInstances {
    private final Instances instances;


    public ClassifyInstances(Instances instances) {
        this.instances = instances;
    }


    public Instances classifyInstances() {
        CostSensitiveClassifier classifier = loadModel();

        try {
            for (int i = 0; i < instances.numInstances(); i++) {
                double label = classifier.classifyInstance(instances.instance(i));
                instances.instance(i).setClassValue(label);
            }
        }
        catch (Exception e){
            System.err.println("Failed to classify instances, please try again.");
            System.exit(0);
        }
        return instances;
    }


    private CostSensitiveClassifier loadModel() {
        try {
            File file = new File("data/CostSensitiveClassifier_NearestNeighbor.model");
            return (CostSensitiveClassifier) SerializationHelper.read(file.getAbsolutePath());
        }
        catch (Exception e) {
            System.err.println("Failed to load model");
            System.exit(0);
        }
        return null;
    }
}