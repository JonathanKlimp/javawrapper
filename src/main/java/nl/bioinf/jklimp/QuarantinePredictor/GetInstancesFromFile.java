package nl.bioinf.jklimp.QuarantinePredictor;

import weka.core.Instances;
import weka.core.converters.CSVLoader;
import weka.core.converters.ConverterUtils.DataSource;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Class which gets the instances from the input file when given.
 */


public class GetInstancesFromFile {

    public Instances getInstanceFromFile(String pathToFile) throws Exception {
        if(pathToFile.endsWith(".csv")) {
            return csvToInstance(pathToFile);
        } else if (pathToFile.endsWith(".arff")){
            return arffToInstance(pathToFile);
        } else {
            System.out.println("Not a valid file, please submit a csv of arff file.");
            System.exit(0);
            return null;
        }
    }


    private Instances csvToInstance(String pathToFile) throws Exception {
        try {
            final InputStream file = new FileInputStream(pathToFile);
            CSVLoader csvLoader = new CSVLoader();
            csvLoader.setSource(file);
            Instances instances = csvLoader.getDataSet();
            return instances;
        } catch (FileNotFoundException e) {
            System.err.println("Could not find: " + pathToFile + " Please try again.");
            return null;
        }

    }


    private Instances arffToInstance(String pathToFile) throws IOException {
        try {
            DataSource source = new DataSource(pathToFile);
            Instances instances = source.getDataSet();
            return instances;
        } catch (FileNotFoundException e) {
            System.err.println("Could not find: " + pathToFile + " Please try again.");
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

}