package nl.bioinf.jklimp.QuarantinePredictor;

/**
 * Main class. Collects the command line arguments and passes them to ProcessCli.
 *
 * @author Jonathan Klimp
 */


public class QuarantinePredictor {
    public static void main(String[] args) {
        /*
          @param args The command line arguments
         */
        try{
            ProcessCli processor = new ProcessCli(args);
            processor.initialize();

        } catch (Exception e){
            System.out.println("Something went wrong with wrong.");
            e.printStackTrace();
        }

    }
}
