package nl.bioinf.jklimp.QuarantinePredictor;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Add;

import java.util.ArrayList;
import java.io.PrintWriter;

/**
 * Class that parses the commandline arguments. Collects the instances from a file or wil create them from
 * the user input. These are classified using ClassifyInstances, and saved to a file or printed based on the user input
 *
 * @author Jonathan Klimp
 */


public class ProcessCli {
    private static final String TYPE = "type";
    private static final String FILE = "file";
    private static final String HELP = "help";

    // singe instance type should contain: Animal, breed, Gender, vaccination years , Bite place
    // variables for each instance
    private static final String ANIMAL = "animal";
    private static final String BREED = "breed";
    private static final String GENDER = "gender";
    private static final String VACCINATIONYEARS = "vaccination years";
    private static final String BITEPLACE = "bite place";

    private static final String OUTPUT = "output";

    private Options options;
    private CommandLine commandLine;
    private final String[] clArguments;

    protected ProcessCli(final String[] args) {
        this.clArguments = args;
    }

    public void initialize() {
        createOptions();
        Instances unclassified = processCommandLine();
        ClassifyInstances classifier = new ClassifyInstances(unclassified);
        Instances instances = classifier.classifyInstances();
        exportInstances(instances);
    }


    private boolean helpRequested() {
        return this.commandLine.hasOption(HELP);
    }


    private void createOptions() {
        this.options = new Options();
        Option helpOption = new Option("h", HELP, false, "Shows help");
        Option typeOption = new Option("t", TYPE, true, "Input type:\n "
                + "For single line type: 'line' \n"
                + "For a file type: 'file'\n");
        Option fileInput = new Option("f", FILE, true,
                "Enter file location when chosen for file");
        Option animalOption = new Option("a", ANIMAL, true,
                "The animal, when chosen for type 'line'");
        Option breedOption = new Option("b", BREED, true,
                "Breed of the animal, when chosen for type 'line'");
        Option genderOption = new Option("g", GENDER, true,
                "Gender of the animal, when chosen for type 'line'");
        Option vaccinationYears = new Option("y", VACCINATIONYEARS, true,
                "If the animal has been vaccinated enter the number of years. " +
                        "If it has not been vaccinated enter 0. When chosen for type 'line'");
        Option bitePlace = new Option("p", BITEPLACE, true,
                "The place of the bite, when chosen for type 'line'");
        Option fileOutput = new Option("o", OUTPUT, true,
                "The format of the output file .csv or .arff." +
                        "If the results should be printed don't include this option");

        options.addOption(helpOption);
        options.addOption(typeOption);
        options.addOption(fileInput);
        options.addOption(animalOption);
        options.addOption(breedOption);
        options.addOption(genderOption);
        options.addOption(vaccinationYears);
        options.addOption(bitePlace);
        options.addOption(fileOutput);
    }


    private Instances processCommandLine() {
        try{
            CommandLineParser parser = new DefaultParser();
            this.commandLine = parser.parse(this.options, this.clArguments);
            if (commandLine.hasOption(TYPE)){
                String type = commandLine.getOptionValue(TYPE).trim();
                return processInstances(type);

            } else if (helpRequested()){
                printHelp();
                System.exit(0);
                return null;
            }
            else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    private void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        final PrintWriter writer = new PrintWriter(System.out);
        formatter.printUsage(writer,80,"Quarantine predictor",options);
        formatter.printHelp("Quarantine classifier", options);
        writer.flush();
    }


    private Instances processInstances(String type) {
        if (commandLine.hasOption(TYPE)) {
            String pathToFile = commandLine.getOptionValue(FILE);
            GetInstancesFromFile getInstancesFromFile = new GetInstancesFromFile();
            try {
                switch (type) {
                    case "file":
                        return setClassLabel(getInstancesFromFile.getInstanceFromFile(pathToFile));
                    case "line":
                        return lineToInstance();
                    default:
                        throw new IllegalArgumentException("Please enter a correct type. " + type
                                + "is not a valid argument. Please enter 'file' or 'line'");
                }
            } catch (Exception e) {
                System.err.println("No correct Type option was submitted, use -h to see the parameters.");
                System.exit(0);
            }
        } else {
            System.out.println("There was no Type option submitted, use -h to see the parameters. please try again");
            System.exit(0);
            return null;
        }
        return null;
    }


    private Instances lineToInstance() throws Exception {
        String animal = commandLine.getOptionValue(ANIMAL);
        String breed = commandLine.getOptionValue(BREED);
        String gender = commandLine.getOptionValue(GENDER);
        String vaccinationYears = commandLine.getOptionValue(VACCINATIONYEARS);
        String vaccinated = getVaccinatedStatus(vaccinationYears);
        String bitePlace = commandLine.getOptionValue(BITEPLACE);

        ArrayList<Attribute> attributes = new ArrayList<>(6);

        attributes.add(new Attribute("Animal", (ArrayList<String>) null));
        attributes.add(new Attribute("Breed", (ArrayList<String>) null));
        attributes.add(new Attribute("Gender", (ArrayList<String>) null));
        attributes.add(new Attribute("Vaccination years", (ArrayList<String>) null));
        attributes.add(new Attribute("Vaccinated", (ArrayList<String>) null));
        attributes.add(new Attribute("Bite place", (ArrayList<String>) null));

        Instances instances = new Instances("Quarantine", attributes, 0);

        double[] quarantineInstance = new double[instances.numAttributes()];
        quarantineInstance[0] = instances.attribute(0).addStringValue(animal);
        quarantineInstance[1] = instances.attribute(1).addStringValue(breed);
        quarantineInstance[2] = instances.attribute(2).addStringValue(gender);
        quarantineInstance[3] = instances.attribute(3).addStringValue(vaccinationYears);
        quarantineInstance[4] = instances.attribute(4).addStringValue(vaccinated);
        quarantineInstance[5] = instances.attribute(5).addStringValue(bitePlace);

        instances.add(new DenseInstance(1, quarantineInstance));

        instances = setClassLabel(instances);
        return instances;
    }


    private Instances setClassLabel(Instances instances) throws Exception {
        Instances unClassified = new Instances(instances);
        Add filter = new Add();
        filter.setAttributeIndex("last");
        filter.setNominalLabels("yes,no");
        filter.setAttributeName("Quarantine");
        filter.setInputFormat(unClassified);
        unClassified = Filter.useFilter(unClassified, filter);

        unClassified.setClassIndex(unClassified.numAttributes() - 1);
        return unClassified;
    }


    private String getVaccinatedStatus(String yrs) {
        String vaccinated;
        if (Integer.parseInt(yrs) == 0) {
            vaccinated = "no";
        } else {
            vaccinated = "yes";
        }
        return vaccinated;
    }


    private void exportInstances(Instances instances){
        if(commandLine.hasOption(OUTPUT)){
            String outputType = commandLine.getOptionValue(OUTPUT);
            WriteToFile fileWriter = new WriteToFile(instances, outputType);
            fileWriter.writeFile();
        } else {
            System.out.println("No file type was submitted, printing the results!");
            System.out.println(instances);
            System.exit(0);
        }
    }


}