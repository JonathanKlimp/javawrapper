package nl.bioinf.jklimp.QuarantinePredictor;

import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.CSVSaver;

import java.io.File;
import java.io.IOException;

/**
 * Class which writes the classified instances to a csv or arff file based on the user input.
 */


public class WriteToFile {
    private final Instances classifiedInstances;
    private final String fileType;


    public WriteToFile(Instances classifiedInstances, String fileType) {
        this.classifiedInstances = classifiedInstances;
        this.fileType = fileType;
    }


    public void writeFile() {
        switch(fileType){
            case "arff":
                writeToArff();
                break;
            case "csv":
                writeToCsv();
                break;
            default:
                System.err.println(fileType + " is not a valid file format, please enter csv or arff");
                System.exit(0);
        }
    }


    private void writeToArff(){
        ArffSaver saver = new ArffSaver();
        saver.setInstances(classifiedInstances);
        try {
            saver.setFile(new File("data/ClassifiedResults.arff"));
            saver.writeBatch();
        } catch (IOException e) {
            System.err.println("Something went wrong with creating the output file");
            e.printStackTrace();
        }
    }


    private void writeToCsv(){
        CSVSaver saver = new CSVSaver();
        saver.setInstances(classifiedInstances);
        try {
            saver.setFile(new File("data/ClassifiedResults.csv"));
            saver.writeBatch();
        } catch (IOException e) {
            System.err.println("Something went wrong with creating the output file");
            e.printStackTrace();
        }
    }
}
