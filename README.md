# Quarantine Predictor #

## Purpose ##
Whenever there is an biting incident involving an animal and a human the animal could be quarantined. This application uses
machine learning to predict wether this animal should be quarantined or not. It uses phenotype data of said animal, wether the animal is vaccinated or not
and the place of the bite its self. 

Version 1.0.0

## Set up ##
This application requires a java version of 8 or higher together with some dependencies: 
- [Apache CLI](https://commons.apache.org/proper/commons-cli/) 1.4 is used to parse the commandline arguments.
- [Weka API](https://waikato.github.io/weka-wiki/use_weka_in_your_java_code/) 3.8.0 is used for classifiying the instances

```
	dependencies{
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.6.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.6.2")
    compile'nz.ac.waikato.cms.weka:weka-stable:3.8.0'
    compile('commons-cli:commons-cli:1.4')
}
```

## How to run ##
The API can process two different types of input either a file (.csv or .arff) or a single line with the instances.
**The .jar currently does not work. The program does work when opened as a IntelliJ project.**

### Program arguments ###
- -t, --type <line/file> Input type can be either a file or a line
- -f, --file <path to file> When choosen for type file the path to the file should be entered here.
- -o, --output <csv/arff> output type can be either csv or arff. Defaults to printing the results
- -a, --animal <animal> When choosen for type line, enter the animal here.
- -b, --animal <breed/UNKNOWN> When choosen for type line, enter the breed of the animal here.
- -g, --gender <MALE/FEMALE/UNKNOWN> When choosen for type line, enter the gender of the animal here.
- -y, --vaccination years <vaccination years> When choosen for type line, enter the number of years the animal has been vaccinated for heere. If the animal was not vaccinated enter 0.
- -p, --bite place <HEAD/BODY> When choosen for type line, enter the place of the bite here.
- -h, --help Shows help.

### File input ###
When you want to use a file with the isntances the type should be set to file. The code block below shows how to run this option
```
$ quarantine-predictor-1.0.0.jar -t file -f <path to file> 
```
The file can either be a csv file or a arff file. The file should has to contain specific column an example of a csv file is shown below.

|SpeciesIDDesc,BreedIDDesc,GenderIDDesc,vaccination_yrs,vaccination,WhereBittenIDDesc|
|------------------------------------------------------------------------------------|
|DOG,UNKNOWN,FEMALE,1,BODY		                      								 |
|DOG,'GERM SHEPHERD',MALE,0,BODY	                                                 |
|CAT,UNKNOWN,MALE,0,BODY							                                 |
|...                                                                                 |   

### Single line input ###
If you want to classify a single animal using the command line the -t argument should be set to line. The code block below shows how to run this option.
```
$ quarantine-predictor-1.0.0.jar -t line -a <animal> -b <breed> -g <gender> -y <vaccination years> -p <bite place>
```
all arguments should be entered with all capital letters. An example is shown below.
```
$ quarantine-predictor-1.0.0.jar -t line -a DOG -b UNKNOWN -g MALE -y 3 -p BODY
```
If the breed of the animal consists of two words the breed should be entered with quotation marks. An example:
```
$ quarantine-predictor-1.0.0.jar -t line -a DOG -b 'GERM SHEPHERD' -g MALE -y 0 -p HEAD
```

### Output ###
Default the application will print the results to the terminal. But the results can be written to an csv or arff file.
To do this the option -o should be added with the argument csv or arff. 
```
$ quarantine-predictor-1.0.0.jar -t line -a DOG -b 'AMER. BULL DOG' -g MALE -y 4 -p BODY -o csv

$ quarantine-predictor-1.0.0.jar -t file -f data/Animal_bites_clean_smote_noclass.csv -o arff
```

### Contact ###
j.klimp@st.hanze.nl

### Extra ####
The research process behind this application can be found in [this repo](https://bitbucket.org/JonathanKlimp/thema09/src/master/)